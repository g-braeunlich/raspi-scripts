# Raspi Scripts

Ansible roles / playbook to setup a raspberry pi for low maintainance,
auto-backup and dynamic dns.

The roles included are:

- `raspbian-base`: Basic configuration (setup of packages, hostname,
   locales, ...)
- `motd`: Setup a message of the day after ssh login
- `debian-low_maintenance`: Ensure unattended upgrades
- `dyndns`: A cron job for sending the current IP to an account on
  http://freedns.afraid.org/
- `msmtp`: Setup msmtp as sendmail implementation
- `backup`: Cronjob for backups of services ([more info](./roles/backup/README.md))

In order to run run the ansible scripts, an initial setup described in
the next section has to be performed.

## Raspbian initial setup

### Download

https://www.raspberrypi.org/downloads/raspbian/

### Install

https://www.raspberrypi.org/documentation/installation/installing-images/linux.md

```bash
unzip -p *-raspbian-stretch.zip \
 | dd status=progress of=/dev/mmcblk0 bs=4M conv=fsync

mount /dev/mmcblk0p1 /boot/
touch /boot/ssh

umount /boot/
mount /dev/mmcblk0p2 /data/
mkdir /data/root/.ssh
cp .ssh/id_rsa.pub /data/root/.ssh/authorized_keys
```

Then boot raspberry pi and ssh into it.

```bash
systemctl enable ssh
```

### Set static IP

```bash
cat <<<EOF > /etc/network/interfaces
auto eth0
iface eth0 inet static
        address 10.0.3.141
        netmask 255.255.252.0
        gateway 10.0.0.254
EOF
systemctl disable dhcpcd
systemctl enable networking
rmdir /etc/network/interfaces.d/
reboot
```

## Run ansible

As preliminary step the following are necessary:

- Register the target host in your `~/.ssh/config` file:
  ```
  Host roundhouse
       User chucknorris
       HostName 192.168.3.141
       Port 22
  ```

- Encrypt sensitive data (e.g. the password for your
  mail account) with ansible vault:

  1. Create a ansible password file:

     ```bash
     echo your_password > .vault_pass
     ```
  2. Encrypt the sudo password
     ```bash
     ansible-vault encrypt_string --vault-id your_id@.vault_pass --name 'msmtp_pass'
     ```

  *Remark:* Everybody with access to the `.vault_pass` file will be able
  to decrypt the password. This method only prevents saving the password
  in plain text. For a setup involwing a password promt, see
  https://docs.ansible.com/ansible/latest/user_guide/vault.html

Now, to run:

```bash
ansible-playbook --vault-id your_id@.vault_pass -i hosts.yml playbook.yml
```
