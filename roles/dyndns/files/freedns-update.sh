#!/bin/bash

URL="${URL:-http://sync.afraid.org/u}"

domains=( $(cat ${HOME}/.config/freedns.conf) )
ip=$(wget -O - "http://ipecho.net/plain" 2>/dev/null)

main_domain="${domains[0]}"
freedns_ip=$(nslookup "${main_domain%:*}" | grep -Po "Address: \K[0-9.]*$")
[[ "$freedns_ip" == "$ip" ]] && exit 1

for dom in "${domains[@]}" ; do
    IFS=":" read domain code <<< "$dom"
    echo "[${domain}]"
    wget -nv -O - "${URL}/${code}/" 2>&1 | grep -i "ERROR:" >&2
    sleep 5
done
