#!/bin/bash

IFS=',' read -ra filenames <<< "$1"
first_entry=1
echo '['
for filename in "${filenames[@]}" ; do
    filename="$(basename $filename)"

    for homedir in /home/* /root/; do
	[[ "$homedir" == '/home/*' ]] && break
	[[ -f "$homedir/$filename" ]] && cmp -s "/etc/skel/$filename" "$homedir/$filename" > /dev/null && continue
	owner_group=$(find $homedir -maxdepth 0 -printf '"owner": "%u", "group": "%g"')
	[[ $first_entry -eq 1 ]] || echo ', '
	first_entry=0
        echo -n "{\"home\": \"${homedir}\", \"file\": \"${filename}\", ${owner_group}}"
    done
done
echo ']'
