# backup

Backup script / cronjob

For each subfolders `<folder>` of `$CONFIG_DIR` (defaults to
`~/.config/backup/`), the script will perform a backup
of the directory `$LOCAL_PATH/<folder>` (`$LOCAL_PATH` defaults to
`/var/backup`).

The backup is performed in 2 steps: *prepare* and *sync*.

By default, there is no *prepare* step and *sync* step assumes, that
the folder `$LOCAL_PATH/<folder>`
is a git repository. In this case, the *sync* step is to commit and
push all changes since the last backup to the git repo's default remote.

If `$CONFIG_DIR/<folder>/sync.sh` exists and is executable, instead of
the default *sync* method (git), `$CONFIG_DIR/<folder>/sync.sh` is
executed.

**Example use case** Use rsync instead of git to sync large binary files.

If `$CONFIG_DIR/<folder>/prepare.sh` exists and is
executable, this executable is run prior to
the *sync* action as the *prepare* step.

**Example use case** SQL dumps of databases (which can be combined
with the default git *sync* method)
