#!/bin/bash

CONFIG_DIR="${CONFIG_DIR:-"${HOME}/.config/backup"}"
LOCAL_PATH="${LOCAL_PATH:-"/var/backup"}"

[[ -z "$REMOTE_URL" ]] && echo 'REMOTE_URL has to be set as an environment variable. Either local path or "ssh://<REMOTE_USR>@<REMOTE_HOST>[:<REMOTE_PORT>]/<REMOTE_PATH>"' >&2 && exit 1

function sync() {
    dir="$1"
    echo "Sync ${dir}"
    [[ -x "${CONFIG_DIR}/${dir}/prepare.sh" ]] && DEST="${LOCAL_PATH}/${dir}" "${CONFIG_DIR}/${dir}/prepare.sh"
    if [[ -x "${CONFIG_DIR}/${dir}/sync.sh" ]] ; then
	typeset -fx create_remote
	typeset -fx create_remote_mounted
	REMOTE_URL="${REMOTE_URL}/${dir}" LOCAL_PATH="${LOCAL_PATH}/${dir}" "${CONFIG_DIR}/${dir}/sync.sh"
    else
	pushd "${LOCAL_PATH}/${dir}" > /dev/null
	git add . && git commit -q -m "$(date)" && git push -f -q origin master
	popd > /dev/null
    fi
}

function create_remote_ssh() {
    dir="$1"
    ssh -p"$ssh_port" "${ssh_user}@${ssh_host}" "[[ -d '${ssh_path}/${dir}' ]]" && return
    echo "Creating remote $dir"
    ssh -p"$ssh_port" "${ssh_user}@${ssh_host}" "mkdir '${ssh_path}/${dir}' && cd '${ssh_path}/${dir}' && git init --bare"
}

function create_remote_mount() {
    dir="$1"
    [[ -d "${REMOTE_URL}/${dir}" ]] && return
    echo "Creating remote ${dir}"
    mkdir "${REMOTE_URL}/${dir}"
    pushd "${REMOTE_URL}/${dir}" > /dev/null
    git init --bare
    popd > /dev/null
}

function create_remote() {
    "create_remote_${sync_method}" "$1"
}

function create_local() {
    dir="$1"
    [[ -d "${LOCAL_PATH}/$dir" ]] && return
    [[ -L "${LOCAL_PATH}/$dir" ]] && return
    echo "Creating local $dir"
    mkdir "${LOCAL_PATH}/$dir"
    pushd "${LOCAL_PATH}/$dir" > /dev/null
    git init
    git remote add origin "${REMOTE_URL}/${dir}"
    popd > /dev/null
}

if [[ $REMOTE_URL == "ssh://"* ]] ; then
    sync_method=ssh
    uri="${REMOTE_URL#ssh://}"
    ssh_user="${uri%@*}"
    uri="${uri#*@}"
    if [[ "$uri" == *:* ]] ; then
	ssh_host="${uri%:*}"
	uri="${uri#*:}"
	ssh_port="${uri%%/*}"
    else
	ssh_host="${uri%%/*}"
	ssh_port=22
    fi
    ssh_path="/${uri#*/}"
else
    sync_method=mount
    mount_path="${MOUNT:-${REMOTE_URL%/*}}"
fi

if [[ -n "$mount_path" ]] ; then
    mount_status=$(mount | grep -q "$mount_path" || echo 0)
    [[ "$mount_status" -eq 0 ]] && mount "$mount_path"
fi

repos=($(ls "${CONFIG_DIR}"))
[[ -n "$1" ]] && repos=( "$1" )
for dir in "${repos[@]}" ; do
    create_remote "$dir"
    create_local "$dir"
    sync "$dir"
done

[[ -n "$mount_path" ]] && [[ "$mount_status" -eq 0 ]] && umount "$mount_path"
